# Contributor: Fabian Affolter <fabian@affolter-engineering.ch>
# Contributor: fossdd <fossdd@pwned.life>
# Maintainer: fossdd <fossdd@pwned.life>
pkgname=ansible-lint
pkgver=24.2.3
pkgrel=1
pkgdesc="check ansible playbooks"
url="https://github.com/ansible/ansible-lint"
arch="noarch"
options="!check"
license="MIT"
depends="
	ansible-core
	black
	git
	py3-ansible-compat
	py3-filelock
	py3-jinja2
	py3-jsonschema
	py3-packaging
	py3-requests
	py3-rich
	py3-ruamel.yaml
	py3-wcmatch
	py3-yaml
	python3
	yamllint
	"
makedepends="
	py3-gpep517
	py3-installer
	py3-setuptools
	py3-setuptools_scm
	py3-wheel
	"
checkdepends="
	py3-flaky
	py3-psutil
	py3-pytest
	py3-pytest-cov
	py3-pytest-xdist
	yamllint
	"
subpackages="$pkgname-pyc"
source="https://pypi.io/packages/source/a/ansible-lint/ansible_lint-$pkgver.tar.gz
	no-version-check.patch
	"
builddir="$srcdir/ansible_lint-$pkgver"
provides="py3-ansible-lint=$pkgver-r$pkgrel" # for backward compatibility
replaces="py3-ansible-lint" # for backward compatibility

build() {
	export SETUPTOOLS_SCM_PRETEND_VERSION=$pkgver
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	pytest
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/ansible_lint-$pkgver-py3-none-any.whl
}

sha512sums="
65d221ef0d64692d1af3fb099a65d2326f7736e8890ef8aae885f341ae6be4d9021c89332ba44b1fc65fc74a815deaaf48e2941ab988e6e464ba45dc540ac6dc  ansible_lint-24.2.3.tar.gz
314fa02e0b30db8a8886824b0cce825ae4ffe227e2c5be434bc96e1c3ab8a6239548574d9ed0869def67b94c684a67abbf594f78aadbc64286fe8187502ba275  no-version-check.patch
"

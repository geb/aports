# Contributor: Lukas Franek <lukas.franek@ui.com>
# Maintainer: Lukas Franek <lukas.franek@ui.com>
pkgname=hpnssh
pkgver=18.4.0
pkgrel=0
pkgdesc="HPNSSH: High performance SSH/SCP"
url="https://psc.edu/hpn-ssh-home"
# s390x: libssh is failing to build
arch="all !s390x"
license="custom"
makedepends="
	autoconf
	automake
	libtool
	linux-headers
	cmake
	openssl-dev>3
	zlib-dev
	"
subpackages="$pkgname-doc"
source="https://github.com/rapier1/hpn-ssh/archive/refs/tags/hpn-$pkgver.tar.gz"
builddir="$srcdir/hpn-ssh-hpn-$pkgver"
# hpnssh-keysign need suid
options="suid"

build() {
	autoreconf -fi
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var \
		--with-mantype=man
	make
}

check() {
	make -j1 file-tests interop-tests unit
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="
b33d5fabf2eaa3524ed624276442c5e4b2ea8e77eb61fcd18371ceea1158ce09c4729107ffc14c345c852d3c76be023c5a288341ec294180aedb5bdf8f97e2bc  hpn-18.4.0.tar.gz
"
